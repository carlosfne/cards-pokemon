import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../../services/pokemon.service';


@Component({
  selector: 'app-lista-baralhos',
  templateUrl: './lista-baralhos.component.html',
  styleUrls: ['./lista-baralhos.component.css']
})
export class ListaBaralhosComponent implements OnInit {
  baralhos: any[] = [];
  cartasPesquisada: any[] = [];
  page:number = 1;
  totalCount!:number;
  nome!:any;
  baralhoCriado: boolean = false;
  termoPesquisa: string = '';
  selecionadas: any[] = [];
  loading: boolean = false;
  showModal: boolean = false;
  nomeBaralho: string = '';
  nomeCarta: string = '';
  baralho: { 
    nome: string, 
    cartas: { 
      nome: string,
      imagem: string,
      tipo: string,
      id: string,
    }[] 
  } = { 
    nome: '', 
    cartas: [] 
  };
  estatisticasBaralho: any[] = [];


  constructor(private pokemonService: PokemonService) { }

  ngOnInit(): void {
  }

  onTermoPesquisaChange() {    
    if (this.termoPesquisa.length >= 3) {
      this.getCards();
    }
    if (this.termoPesquisa.length === 0) {
      this.loading = false;
      this.cartasPesquisada = [];
      return
    }
  }

  getCards() {
    this.loading = true;
    this.pokemonService.obterCartas(this.page).subscribe((response: any) => {
       this.cartasPesquisada = response.data.filter((carta: any) =>
          carta.name.toLowerCase().includes(this.termoPesquisa.toLowerCase())
       );
       this.totalCount = response.data.totalCount;
       this.loading = false;
    });
  }

  criarNovoBaralho(nomeBaralho:string): void {   
    const novoBaralho = { 
      nome: this.nomeBaralho, 
      cartas: [] 
    };
    this.baralhoCriado = true; 
    this.baralho.nome = this.nomeBaralho; 
    this.baralhos.push(novoBaralho);
    this.nomeBaralho = '';
  }

  removerBaralho(id: string): void {
    this.baralho.cartas = this.baralho.cartas.filter(carta => carta.id !== id);
  }

  adicionarAoBaralho(carta: any, idBaralho: any): void {
    const baralho = this.baralhos[idBaralho];
    if (this.baralho.cartas.filter(c => c.nome === carta.name).length < 4) {
        this.baralho.cartas.push({ 
          nome: carta.name, 
          imagem: carta.images.large, 
          tipo: carta.types[0],
          id: carta.id
        });
        this.estatisticasBaralho[idBaralho] = this.calcularEstatisticasBaralho(this.baralho);
    } else {
        alert('Só podem existir 4 cartas com o mesmo nome no baralho!');
    }
  }

  toggleModal(carta?:any){
    this.showModal = !this.showModal;
  }

  calcularEstatisticasBaralho(baralho: any) {
    const tiposUnicos = new Set<string>();
    let pokemons = 0;
    let cartasTreinador = 0;
    const cartasDoBaralho = baralho.cartas;

    cartasDoBaralho.forEach((carta: any) => {
      if (carta.supertype === 'Pokémon') {
        pokemons++;
        tiposUnicos.add(carta.types[0]);
      } else if (carta.supertype === 'Trainer') {
        cartasTreinador++;
      }
    });

    return {
      totalPokemons: pokemons,
      totalCartasTreinador: cartasTreinador,
      totalCores: tiposUnicos.size,
    };
  }

}
