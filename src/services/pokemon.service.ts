import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  private apiUrl = 'https://api.pokemontcg.io/v2/cards';
  baralhos: any[] = [];

  constructor(private http: HttpClient) { }

  obterCartas(page:number): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}?page=${page}`);
  }

  obterBaralhos(): any[] {
    return this.baralhos;
  }

  criarBaralho(nome: string): void {
    const novoBaralho = {
      id: this.baralhos.length + 1, 
      nome: nome,      
    };
    this.baralhos.push(novoBaralho);
  }

  removerBaralho(id: string): void {
    this.baralhos = this.baralhos.filter(baralho => baralho.id !== id);
  }
}
